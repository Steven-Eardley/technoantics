# Technoantics Website
This work is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

## 1. Deployment

This is managed using git hooks. You'll need the server in your ```.ssh/config``` file, and the remote:

    git remote add production user@server:technoantics.git

You can only deploy the master branch. To do so, run the following command:

    git push production master

i.e. you are pushing master to the production remote. This will run the hooks, and restart ```nginx``` for you.

If you want to execute the hooks again without changing the code, you'll need an empty commit, then push to production:

    git commit --allow-empty -m "trigger deploy"
    git push production master

## 2. Server configuration

Requires a bare git repo on the host, called `technoantics.git`. This is created with
`git init --bare technoantics.git` in the home directory.

Create the working tree directory where nginx will serve the files from and fix the permissions:

    sudo mkdir /var/www/technoantics
    sudo chown www-data:www-data /var/www/technoantics
    sudo chmod -R 775 /var/www
    sudo usermod -a -G www-data user

The post receive hook then needs to be created on the host, by copying `deploy/hooks/post-receive` to
`~/technoantics.git/hooks/post-receive` and `chmod +x hooks/post-receive` on the host. This will allow the script to
run on deployment and copy the code to the correct work tree.

Run a code deploy from your local machine to receive the source tree for the first time.

    git push production master

After the first deploy, replace the copied hook with a symlink to the checked out hook (so you can update the hook):

    ln -sf /var/www/technoantics/deploy/hooks/post-receive hooks/post-receive
    chmod +x hooks/post-receive
    chmod +x /var/www/technoantics/deploy/hooks/post-receive

## 3. SSL Certificates

The nginx config in `deploy/nginx` expects an SSL certificate - this should be created via LetsEncrypt / certbot.

For your SSL certificate to validate, the site must be accessible via nginx; create the symlinks to the temporary
nginx config:

    sudo ln -s /var/www/technoantics/deploy/nginx/init.technoantics.com /etc/nginx/sites-available/technoantics.com
    sudo ln -s /etc/nginx/sites-available/technoantics.com /etc/nginx/sites-enabled/technoantics.com
    sudo nginx -s reload

Run certbot:

    sudo certbot                                                                           # And follow the instructions

It will make changes to the initialisation config, discard these changes so the source tree is clean.

    git --work-tree=/var/www/technoantics --git-dir=/home/steve/technoantics.git reset --hard

Then switch over to the production nginx config:
    sudo ln -sf /var/www/technoantics/deploy/nginx/init.technoantics.com /etc/nginx/sites-available/technoantics.com
    sudo nginx -s reload
